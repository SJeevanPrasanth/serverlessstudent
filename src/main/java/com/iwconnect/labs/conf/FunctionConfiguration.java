package com.iwconnect.labs.conf;

import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.iwconnect.labs.student.domain.Student;
import com.iwconnect.labs.student.domain.Teacher;
import com.iwconnect.labs.student.dto.StudentPojo;
import com.iwconnect.labs.student.dto.TeacherPojo;
import com.iwconnect.labs.student.service.StudentService;
import com.iwconnect.labs.util.Utils;

import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Supplier;

@Configuration
public class FunctionConfiguration {
	private static Logger logger = LoggerFactory.getLogger(FunctionConfiguration.class);

	@Autowired
	StudentService studentService;
	
	@Bean
	public Function<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> createStudent() {
		logger.info("Execute Lambda createStudent");

		return value -> {
			try {
				ObjectMapper mapper = new ObjectMapper();
				StudentPojo studentPojo = Utils.studentMapper(value, mapper);
				Student student = studentService.saveStudent(studentPojo);

				return createResponseEvent(student);
			} catch (Exception e) {
				logger.error("Error executing createStudent function", e);
				e.printStackTrace();
				return new APIGatewayProxyResponseEvent().withStatusCode(500);
			}
		};
	}

	@Bean
	public Function<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> createTeacher() {
		logger.info("Execute Lambda createTeacher");

		return value -> {
			try {
				ObjectMapper mapper = new ObjectMapper();
				TeacherPojo teacherPojo = Utils.TeacherMapper(value, mapper);
				Teacher teacher = studentService.saveTeacher(teacherPojo);

				return createResponseEvent(teacher);
			} catch (Exception e) {
				logger.error("Error executing createTeacher function", e);
				e.printStackTrace();
				return new APIGatewayProxyResponseEvent().withStatusCode(500);
			}
		};
	}

	private APIGatewayProxyResponseEvent createResponseEvent(Student student) {
		logger.info("Execute createResponseEvent method");
		APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
		ObjectMapper mapper = new ObjectMapper();
		try {
			responseEvent.setStatusCode(201);
			responseEvent.setHeaders(createResultHeader());
			responseEvent.setBody(mapper.writeValueAsString(student));
		} catch (Exception e) {
			logger.error("Error executing createResponseEvent method", e);
			return new APIGatewayProxyResponseEvent().withStatusCode(500);
		}
		return responseEvent;
	}

	private APIGatewayProxyResponseEvent createResponseEvent(Teacher teacher) {
		logger.info("Execute createResponseEvent method");
		APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
		ObjectMapper mapper = new ObjectMapper();
		try {
			responseEvent.setStatusCode(201);
			responseEvent.setHeaders(createResultHeader());
			responseEvent.setBody(mapper.writeValueAsString(teacher));
		} catch (Exception e) {
			logger.error("Error executing createResponseEvent method", e);
			return new APIGatewayProxyResponseEvent().withStatusCode(500);
		}
		return responseEvent;
	}

	@Bean
	public Supplier<APIGatewayProxyResponseEvent> getAllStudents(){
	 return ()->{
	 APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
	try {
	 List<StudentPojo> response = studentService.getAllStudents();
	 responseEvent.setBody(response.toString());
	 responseEvent.setStatusCode(HttpStatus.SC_OK);
	 return responseEvent;
	 }
	catch (Exception e) {
	 e.printStackTrace();
	 return new APIGatewayProxyResponseEvent().withStatusCode(500);
	 }
	};
	}

//	@Bean
//	public Supplier<APIGatewayProxyResponseEvent> getStudentById(@PathVariable String uuid){
//		return ()->{
//			APIGatewayProxyResponseEvent responseEvent = new APIGatewayProxyResponseEvent();
//			try {
//				StudentPojo response = studentService.getStudentById(uuid);
//				responseEvent.setBody(response.toString());
//				responseEvent.setStatusCode(HttpStatus.SC_OK);
//				return responseEvent;
//			}
//			catch (Exception e) {
//				e.printStackTrace();
//				return new APIGatewayProxyResponseEvent().withStatusCode(500);
//			}
//		};
//	}


	private Map<String, String> createResultHeader() {
		logger.info("Execute createResultHeader method");
		Map<String, String> resultHeader = new HashMap<>();
		resultHeader.put("Content-Type", "application/json");

		return resultHeader;
	}
}