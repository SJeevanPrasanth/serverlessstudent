package com.iwconnect.labs.student.repository;

import com.iwconnect.labs.student.domain.Student;
import com.iwconnect.labs.student.domain.Teacher;
import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;

@EnableScan
public interface TeacherRepository extends CrudRepository<Teacher, String> {
}
