package com.iwconnect.labs.student.dto;

public class StudentPojo {
	private String firstName;
	private String lastName;

	private TeacherPojo teacherPojo;

	public TeacherPojo getTeacherPojo() {
		return teacherPojo;
	}

	public void setTeacherPojo(TeacherPojo teacherPojo) {
		this.teacherPojo = teacherPojo;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "StudentPojo{" +
				"firstName='" + firstName + '\'' +
				", lastName='" + lastName + '\'' +
				", teacherPojo=" + teacherPojo +
				'}';
	}
}
