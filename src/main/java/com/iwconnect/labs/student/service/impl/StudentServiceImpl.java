package com.iwconnect.labs.student.service.impl;

import com.iwconnect.labs.student.domain.Teacher;
import com.iwconnect.labs.student.dto.TeacherPojo;
import com.iwconnect.labs.student.repository.TeacherRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.iwconnect.labs.student.domain.Student;
import com.iwconnect.labs.student.dto.StudentPojo;
import com.iwconnect.labs.student.repository.StudentRepository;
import com.iwconnect.labs.student.service.StudentService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImpl implements StudentService {
	private static Logger logger = LoggerFactory.getLogger(StudentServiceImpl.class);

	@Autowired
	StudentRepository studentRepository;

	@Autowired
	TeacherRepository teacherRepository;

	@Override
	public Student saveStudent(StudentPojo studentPojo) throws Exception {
		logger.info("Saving student {} into DB", studentPojo);
		Student transientStudent = new Student();
		try {
			// mapping attributes
			transientStudent.setFirstName(studentPojo.getFirstName());
			transientStudent.setLastName(studentPojo.getLastName());
			// save to DB
			return studentRepository.save(transientStudent);
		} catch (Exception e) {
			logger.error("Error saving student ", e);
			throw e;
		}
	}

	@Override
	public Teacher saveTeacher(TeacherPojo teacherPojo) {
		logger.info("Saving teacher {} into DB", teacherPojo);
		Teacher transientStudent = new Teacher();
		try {
			// mapping attributes
			transientStudent.setFirstName(teacherPojo.getFirstName());
			transientStudent.setLastName(teacherPojo.getLastName());
			// save to DB
			return teacherRepository.save(transientStudent);
		} catch (Exception e) {
			logger.error("Error saving teacher ", e);
			throw e;
		}
	}

	@Override
	public List<StudentPojo> getAllStudents() {
		List<Student> all= (List<Student>) studentRepository.findAll();
		List<StudentPojo> studentPojos=new ArrayList<>();
		for(Student students:all){
			StudentPojo studentPojo=new StudentPojo();
			studentPojo.setFirstName(students.getFirstName());
			studentPojo.setLastName(students.getLastName());
			studentPojos.add(studentPojo);
		}

	return studentPojos;
	}

//	@Override
//	public StudentPojo getStudentById(String uuid) {
//
//		Optional<Student> student=studentRepository.findById(uuid.toString());
//		StudentPojo studentPojo = new StudentPojo();
//		if (student.isPresent()) {
//			BeanUtils.copyProperties(student.get(), studentPojo);
//			return studentPojo;
//		} else {
//			throw new RuntimeException("Sprint details do not exist with Id" + uuid);
//		}
//	}
}