package com.iwconnect.labs.student.service;

import com.iwconnect.labs.student.domain.Student;
import com.iwconnect.labs.student.domain.Teacher;
import com.iwconnect.labs.student.dto.StudentPojo;
import com.iwconnect.labs.student.dto.TeacherPojo;

import java.util.List;

public interface StudentService {
	public Student saveStudent(StudentPojo responseData) throws Exception;

    public Teacher saveTeacher(TeacherPojo teacherPojo);

    List<StudentPojo> getAllStudents();

//    StudentPojo getStudentById(String uuid);
}
